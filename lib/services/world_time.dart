import 'package:http/http.dart';
import 'dart:convert';
import 'package:intl/intl.dart';

class WorldTime {

  String location; // location name for the UI
  late String time; // time in that location
  String flag; // usr to an asset flag icon
  String url; // location url for api endpoint
  bool? isDayTime; //

  WorldTime({required this.location, required this.flag, required this.url});

  Future<void> getTime() async {

    try {
      // make the request
      Response response = await get(Uri.parse('https://worldtimeapi.org/api/timezone/$url'));
      Map data = jsonDecode(response.body); // convert json data to Map object and store locally
      String datetime = data['datetime']; // get property from data
      String offset = data['utc_offset'];
      offset = offset.replaceAll("+", "");

      //create DateTime object
      DateTime now = DateTime.parse(datetime);
      now = now.add(Duration(
          hours: int.parse(offset.split(':')[0]),
          minutes: int.parse(offset.split(':')[1])));
      //DateTime now = DateTime.parse(datetime).toLocal(); // converting the datetime to local time
      isDayTime = now.hour > 6 && now.hour < 20 ? true : false ;
      time = DateFormat.jm().format(now); // set the time property
    }
    catch (e) {
      time = 'Could not get time';
    }
  }
}