import 'package:world_time/services/world_time.dart';
import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';

class Continents {
  late List<WorldTime> continent;
  String val;
  final height;
  final width;
  void Function(WorldTime)? updateTime;

  Continents({required this.val, required this.updateTime, required this.height, required this.width});

  DropdownButtonHideUnderline setContinent(continent) {
  return DropdownButtonHideUnderline(
    child: DropdownButton2<String>(
      isExpanded: true,
      hint: Row(
        children: [
         const Icon(
          Icons.list,
          size: 16,
          color: Colors.yellow,
         ),
       const SizedBox(
         width: 6,
        ),
         Expanded(
           child: Text(
            val,
             style: const TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Colors.amber,
               ),
          overflow: TextOverflow.ellipsis,
          ),
        ),
       ],
      ),
    items: continent.map<DropdownMenuItem<String>>((WorldTime item)  {
      return DropdownMenuItem(
    //items: continent.map((WorldTime item) => DropdownMenuItem<String>(
      value: item.location,
      child: Center(
        child: Text(
          item.location,
          style: const TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.bold,
          color: Colors.lightGreenAccent,
        ),
          overflow: TextOverflow.ellipsis,
        ),
      ),
      onTap: (){
        updateTime!(item);
      },
    );}).toList(),

    onChanged: (value) {},
    buttonStyleData: ButtonStyleData(
      height: height * 0.095,
      width: width *1,
      padding: const EdgeInsets.only(left: 14, right: 14),
      decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(14),
      border: Border.all(
        color: Colors.white,
      ),
      color: Colors.black87,
      ),
      elevation: 2,
    ),
    iconStyleData: const IconStyleData(
      icon: Icon(
        Icons.arrow_forward_ios_outlined,
      ),
      iconSize: 14,
      iconEnabledColor: Colors.yellow,
      iconDisabledColor: Colors.grey,
     ),
    dropdownStyleData: DropdownStyleData(
      maxHeight: height * 1,
      width: width * 1,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(14),
        color: Colors.black,
      ),
      offset: const Offset(-20, 0),
      scrollbarTheme: ScrollbarThemeData(
        radius: const Radius.circular(40),
        thickness: MaterialStateProperty.all(6),
        thumbVisibility: MaterialStateProperty.all(true),
        ),
    ),
    menuItemStyleData: const MenuItemStyleData(
      height: 40,
      padding: EdgeInsets.only(left: 14, right: 14),
      ),
    ),
  );
 }
}