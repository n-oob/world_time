import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Map data = {};
  //Object? parameters;

  @override
  Widget build(BuildContext context) {
    data = data.isNotEmpty ? data : ModalRoute.of(context)!.settings.arguments as Map;

    // set bg
    String bgImage = data['isDayTime'] ? 'day.png' : 'night.png' ;
    Color? bgColor = data['isDayTime'] ? Colors.blue[400] : Colors.indigo[700] ;
    
    return Scaffold(
      backgroundColor: bgColor,
      body: SafeArea(
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/$bgImage'),
                fit: BoxFit.cover,
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 180, 0, 0),
              child: Center(
                child: Column(
                  children: <Widget>[
                    TextButton.icon(
                        onPressed: () async {
                          dynamic result = await Navigator.pushNamed(context, '/location');
                          setState(() {
                            data = {
                              'time': result['time'],
                              'location': result['location'],
                              'isDayTime': result['isDayTime'],
                              'flag': result['flag']
                            };
                          });
                        },
                        icon: Icon(
                          Icons.location_on,
                          color: Colors.amber[400],
                        ),
                        label: Text('set location')
                    ),
                    const SizedBox(height: 20.0,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          data['location'],
                          style: const TextStyle(
                            color: Colors.amber,
                            fontSize: 28.0,
                            letterSpacing: 2.0,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 40.0,),
                    Text(
                      data['time'],
                      style: const TextStyle(
                        color: Colors.amber,
                        fontSize: 66.0,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
      ),
    );
  }
}
